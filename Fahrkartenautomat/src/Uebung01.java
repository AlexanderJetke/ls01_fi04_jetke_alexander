
public class Uebung01 {

	public static void main(String[]args) {
		
		int x = 7;
		int y = 8;
		double m = berechneMittelwert(x, y);
		System.out.println(m);
	}

	private static double berechneMittelwert(int x, int y) {
		
		 return (x + y) / 2.0; 
	
	}
	
}
