﻿import java.util.Scanner;

class Fahrkartenautomat {
	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		double zuZahlenderBetrag;
		double eingezahlterGesamtbetrag;

		do {
			zuZahlenderBetrag = fahrkartenbestellungErfassen();
			eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
			fahrkartenAusgeben();
			rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);

			System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
					+ "Wir wünschen Ihnen eine gute Fahrt.\n");

		} while (true);

	}

	private static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
		// TODO Auto-generated method stub
		double rückgabebetrag;
		int[][] ausgabeMünzen = new int[6][1];
		boolean arrayNotEmpty = true;

		rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		if (rückgabebetrag > 0.0) {
			System.out.printf("Der Rückgabebetrag in Höhe von %.2f €\n", rückgabebetrag);
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				ausgabeMünzen[0][0] = +1;
				muenzeAusgeben(2, "EURO");
				rückgabebetrag -= 2.0;
			}
			while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				ausgabeMünzen[1][0] = +1;
				muenzeAusgeben(1, "EURO");
				rückgabebetrag -= 1.0;
			}
			while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				ausgabeMünzen[2][0] = +1;
				muenzeAusgeben(50, "Cent");
				rückgabebetrag -= 0.5;
			}
			while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				ausgabeMünzen[3][0]= +1;
				muenzeAusgeben(20, "Cent");
				rückgabebetrag -= 0.2;
			}
			while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				ausgabeMünzen[4][0] = +1;
				muenzeAusgeben(10, "Cent");
				rückgabebetrag -= 0.1;
			}
			while (rückgabebetrag >= 0.04)// 5 CENT-Münzen
			{
				ausgabeMünzen[5][0] = +1;
				muenzeAusgeben(5, "Cent");
				rückgabebetrag -= 0.05;
			}
	/*		while (arrayNotEmpty) {
				for (int i = 0; i < ausgabeMünzen.length; i++) {
					while (ausgabeMünzen[i][0] <= 3) {
						if(i <= 1 ) {
							muenzeAusgeben3(ausgabeMünzen[i][0], "Euro");
						}
						else
						ausgabeMünzen[i][0] = -3;
					}
					while(ausgabeMünzen[i][0] <= 2) {
						muenzeAusgeben3(2, "Euro");
						ausgabeMünzen[i][0] = -2;
						
					}
				}
			} */ 
		}

	}

	private static void fahrkartenAusgeben() {
		// TODO Auto-generated method stub

		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			warte(250);
		}
		System.out.println("\n\n");
	}

	public static double fahrkartenbestellungErfassen() {
		Scanner tastatur = new Scanner(System.in);
		double zuZahlenderBetrag = 0;
		int anfrageTickets = 0;
		int ihreWahlTickets = 0;
		boolean falscheAuswahl = true;
		boolean nichtbezahlen = true;
		boolean anzahlStimmtNicht = true;
		double zuZahlenderBetrag2 = 0;
		
		String[] fahrkartenBezeichnung = new String[10];
		double[] fahrkartenPreis = new double[10];

		fahrkartenBezeichnung[0] = "Einzelfahrschein Berlin AB";
		fahrkartenBezeichnung[1] = "Einzelfahrschein Berlin BC";
		fahrkartenBezeichnung[2] = "Einzelfahrschein Berlin ABC";
		fahrkartenBezeichnung[3] = "Kurzstrecke";
		fahrkartenBezeichnung[4] = "Tageskarte Berlin AB";
		fahrkartenBezeichnung[5] = "Tageskarte Berlin BC";
		fahrkartenBezeichnung[6] = "Tageskarte Berlin ABC";
		fahrkartenBezeichnung[7] = "Kleingruppen-Tageskarte Berlin AB";
		fahrkartenBezeichnung[8] = "Kleingruppen-Tageskarte Berlin BC";
		fahrkartenBezeichnung[9] = "Kleingruppen-Tageskarte Berlin ABC";

		fahrkartenPreis[0] = 2.90;
		fahrkartenPreis[1] = 3.30;
		fahrkartenPreis[2] = 3.60;
		fahrkartenPreis[3] = 1.90;
		fahrkartenPreis[4] = 8.60;
		fahrkartenPreis[5] = 9.00;
		fahrkartenPreis[6] = 9.60;
		fahrkartenPreis[7] = 23.50;
		fahrkartenPreis[8] = 24.30;
		fahrkartenPreis[9] = 24.90;

		while (nichtbezahlen) {
			falscheAuswahl = true;
			System.out.println("Wählen Sie aus:");

			for (int i = 0; i < fahrkartenBezeichnung.length; i++) {
				System.out.println(i + 1 + ". " + fahrkartenBezeichnung[i] + " " + fahrkartenPreis[i] + "0€");

			}
			System.out.println("9999. Möchten Sie bezahlen?");
			falscheAuswahl = true;
			while (falscheAuswahl) {

				ihreWahlTickets = tastatur.nextInt();

				if (ihreWahlTickets <= fahrkartenBezeichnung.length && ihreWahlTickets >= 1) {
					falscheAuswahl = false;
					anzahlStimmtNicht = true;
					zuZahlenderBetrag2  = ( fahrkartenPreis[ihreWahlTickets - 1]);
				} else if (ihreWahlTickets == 9999) {
					falscheAuswahl = false;
					nichtbezahlen = false;
					anzahlStimmtNicht = false;
				} else {
					anzahlStimmtNicht = true;
					System.out.print("Falsche eingabe, bitte wählen Sie ein Tiket oder bezahlen!");
				}
			}

			while (anzahlStimmtNicht) {

				System.out.print("Anzahl Tickets eingeben 1 bis 10 Tikets:");
				anfrageTickets = tastatur.nextInt();

				if (anfrageTickets >= fahrkartenBezeichnung.length) {
					anzahlStimmtNicht = true;
					nichtbezahlen = false;

					System.out.println("Anzahl Tickets ist größer 10!");

				} else if (anfrageTickets <= 0) {
					anzahlStimmtNicht = true;
					System.out.println("Anzahl Tickets ist kleiner 1!");

				} else {
					anzahlStimmtNicht = false;
					zuZahlenderBetrag += zuZahlenderBetrag2 * anfrageTickets;
					System.out.println("Anzahl Tickets passt!");
				}
			}

			System.out.printf("Momentan zu Zahlender Preis: %.2f €\n", zuZahlenderBetrag);

		}
		return zuZahlenderBetrag;
	}

	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		Scanner tastatur = new Scanner(System.in);
		double eingezahlterGesamtbetrag;
		double eingeworfeneMünze;

		eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			double ausstehenderBetrag = zuZahlenderBetrag - eingezahlterGesamtbetrag;
			System.out.printf("Noch zu zahlen: %.2f €\n", ausstehenderBetrag);
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");

			boolean einwurfStimmtNicht = true;

			while (einwurfStimmtNicht) {
				eingeworfeneMünze = tastatur.nextDouble();
				if (eingeworfeneMünze <= 2 && eingeworfeneMünze >= 0.05) {
					eingezahlterGesamtbetrag += eingeworfeneMünze;
					einwurfStimmtNicht = false;
				} else {
					System.out.print("Eingabe stimmt nicht, bitte wiederholen (mind. 5Ct, höchstens 2 Euro): ");
				}

			}
		}
		return eingezahlterGesamtbetrag;

	}

	public static void warte(int millisekunde) {
		try {
			Thread.sleep(millisekunde);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void muenzeAusgeben(int betrag, String einheit) {
		System.out.println("      * * *");
		System.out.println("   *        *");
		if (betrag <= 9) {
			System.out.println("  *     " + betrag + "    *");
		}

		else {
			System.out.println("  *    " + betrag + "    *");
		}
		System.out.println("  *   " + einheit + "   *");
		System.out.println("   *        *");
		System.out.println("      * * *");

		// System.out.println(betrag + " " + einheit);
	}

	public static void muenzeAusgeben2(int betrag, String einheit) {
		System.out.println("      * * *" + "     " + "      * * *");
		System.out.println("   *        *" + "     " + "   *        *");
		if (betrag <= 9) {
			System.out.println("  *     " + betrag + "    *" + "     " + "  *     " + betrag + "    *");
		}

		else {
			System.out.println("  *    " + betrag + "    *" + "     " + "  *    " + betrag + "    *");
		}
		System.out.println("  *   " + einheit + "   *" + "     " + "  *   " + einheit + "   *");
		System.out.println("   *        *" + "     " + "   *        *");
		System.out.println("      * * *" + "     " + "      * * *");

		// System.out.println(betrag + " " + einheit);
	}

	public static void muenzeAusgeben3(int betrag, String einheit) {
		System.out.println("      * * *");
		System.out.println("   *        *");
		if (betrag <= 9) {
			System.out.println("  *     " + betrag + "    *");
		}

		else {
			System.out.println("  *    " + betrag + "    *");
		}
		System.out.println("  *   " + einheit + "   *");
		System.out.println("   *        *");
		System.out.println("      * * *");

		// System.out.println(betrag + " " + einheit);
	}
}