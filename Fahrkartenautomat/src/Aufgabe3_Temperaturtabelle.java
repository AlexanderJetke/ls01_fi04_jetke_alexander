
public class Aufgabe3_Temperaturtabelle {

	public static void main(String[] args) {
		String s = "Fahrenheit";
		String u = "Celsius";

		System.out.printf("|%-12s|", s);
		System.out.printf("%10s|\n", u);
		System.out.println("-------------------------");
		System.out.printf("%+-12d|%4s%.2f\n", -20, "", -28.8889);
		System.out.printf("%+-12d|%4s%.2f\n", -10, "", -23.3333);
		System.out.printf("%+-12d|%4s%.2f\n", 0, "", -17.7778);
		System.out.printf("%+-12d|%4s%.2f\n", 10, "", -17.7778);
		System.out.printf("%+-12d|%5s%.2f\n", 20, "", -6.6667);
		System.out.printf("%+-12d|%5s%.2f\n", 30, "", -1.1111);

	}

}
